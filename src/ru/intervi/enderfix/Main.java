package ru.intervi.enderfix;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.entity.Player;
import org.bukkit.Achievement;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.block.Block;
import org.bukkit.Material;
import org.bukkit.World;

public class Main extends JavaPlugin implements Listener {
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
		System.out.println("<--- Created by InterVi --->");
	}
	
	private int hash = "the_end".hashCode();
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onMove(PlayerMoveEvent event) {
		Location to = event.getTo();
		if (event.getFrom().distance(to) >= 1) { //если игрок перешел на другой блок
			Player player = event.getPlayer();
			World world = to.getWorld();
			String wname = world.getName().toLowerCase();
			int hname = wname.substring((wname.indexOf('_')+1), wname.length()).hashCode();
			if (hname == hash) { //если измерение - край (нубопроверка)
				Block block = to.getBlock();
				if (!block.isEmpty()) {
					if (block.getType().equals(Material.ENDER_PORTAL)) { //если игрок попал в портал
						event.setCancelled(true);
						if (player.hasAchievement(Achievement.END_PORTAL) && !player.hasAchievement(Achievement.THE_END)) {
							player.awardAchievement(Achievement.THE_END); //выдача ачивки
						}
						Location bed = player.getBedSpawnLocation();
						if (bed != null) player.teleport(bed, TeleportCause.UNKNOWN); //телепортация
						else player.teleport(world.getSpawnLocation(), TeleportCause.UNKNOWN);
					}
				}
			}
		}
	}
}